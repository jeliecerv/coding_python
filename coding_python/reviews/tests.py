# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from coding_python.reviews.models import Company, Review
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User


class UserTests(APITestCase):
    '''
    Test cases to check the User endpoints
    '''
    def create_user(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    def test_create_user(self):
        self.create_user()
        token = Token.objects.get(user__username='john')

        url = reverse('reviews-api:user-list')
        data = {'username': 'joe', 'email': 'joe@doe.com', 'password': 'joe123456'}
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(url, data, format='json')

        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(User.objects.filter(username='joe').count(), 0)

    def test_list_users(self):
        self.create_user()
        token = Token.objects.get(user__username='john')

        url = reverse('reviews-api:user-list')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(User.objects.all().count(), 0)

    def test_get_user(self):
        self.create_user()
        token = Token.objects.get(user__username='john')

        user = User.objects.all().first()
        url = reverse('reviews-api:user-detail', kwargs={'pk': user.id})
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CompanyTests(APITestCase):
    '''
    Test cases to check the Company endpoints
    '''
    def create_user(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    def test_create_company(self):
        self.create_user()
        token = Token.objects.get(user__username='john')

        url = reverse('reviews-api:company-list')
        data = {'name': 'Company unit test'}
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Company.objects.count(), 1)
        self.assertEqual(Company.objects.get().name, 'Company unit test')

    def test_list_companies(self):
        self.test_create_company()
        token = Token.objects.get(user__username='john')

        url = reverse('reviews-api:company-list')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(Company.objects.all().count(), 0)

    def test_get_company(self):
        self.test_create_company()
        token = Token.objects.get(user__username='john')

        company = Company.objects.all().first()
        url = reverse('reviews-api:company-detail', kwargs={'pk': company.id})
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_company(self):
        self.test_create_company()
        token = Token.objects.get(user__username='john')

        company = Company.objects.all().first()
        url = reverse('reviews-api:company-detail', kwargs={'pk': company.id})
        data = {'name': 'Company unit test update'}
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Company.objects.count(), 1)
        self.assertNotEqual(Company.objects.get().name, 'Company unit test')


class ReviewTests(APITestCase):
    '''
    Test cases to check the Review endpoints
    '''
    def create_user(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    def create_company(self):
        return Company.objects.create(name='Company Paul')

    def test_create_review(self):
        self.create_user()
        company = self.create_company()
        token = Token.objects.get(user__username='john')

        url = reverse('reviews-api:review-list')
        data = {
            'title': 'Test review',
            'summary': 'test summary',
            'rating': 3,
            'company': company.id
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Review.objects.count(), 1)
        self.assertEqual(Review.objects.get().title, 'Test review')

    def test_list_reviews(self):
        self.test_create_review()
        token = Token.objects.get(user__username='john')

        url = reverse('reviews-api:review-list')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(Review.objects.all().count(), 0)

    def test_get_review(self):
        self.test_create_review()
        token = Token.objects.get(user__username='john')

        review = Review.objects.all().first()
        url = reverse('reviews-api:review-detail', kwargs={'pk': review.id})
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_review(self):
        self.test_create_review()
        company = self.create_company()
        token = Token.objects.get(user__username='john')

        review = Review.objects.all().first()
        url = reverse('reviews-api:review-detail', kwargs={'pk': review.id})
        data = {
            'title': 'Test review Update',
            'summary': 'test summary Update',
            'rating': 5,
            'company': company.id
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Review.objects.count(), 1)
        self.assertNotEqual(Review.objects.get().title, 'Test review')

    def test_create_review_wrong_rating(self):
        self.create_user()
        company = self.create_company()
        token = Token.objects.get(user__username='john')

        url = reverse('reviews-api:review-list')
        data = {
            'title': 'Test review',
            'summary': 'test summary',
            'rating': 6,
            'company': company.id
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = self.client.post(url, data, format='json')

        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Review.objects.count(), 0)
