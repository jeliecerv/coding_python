from django.conf.urls import url, include
from rest_framework import routers
from . import views

app_name = 'reviews'

router = routers.DefaultRouter()
router.register(r'reviews', views.ReviewViewSet)
router.register(r'companies', views.CompanyViewSet)
router.register(r'users', views.UserViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
]
