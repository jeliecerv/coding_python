from rest_framework import serializers
from coding_python.reviews.models import Review, Company
from django.contrib.auth.models import User


class CompanySerializer(serializers.ModelSerializer):
    '''
    Serializer of Company Model to present the information on the correct way
    '''
    class Meta:
        model = Company
        fields = ('id', 'name')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    '''
    Serializer of User Model to present the information on correct way
    '''
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class ReviewSerializer(serializers.ModelSerializer):
    '''
    Serializer of Review Model to present the information on the correct way
    '''
    company = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Company.objects.all()
    )

    class Meta:
        model = Review
        fields = ('id', 'title', 'summary', 'rating', 'created_at',
                  'updated_at', 'ip_address', 'company')
        read_only_fields = ('ip_address', 'reviewer')

    def create(self, validated_data):
        ip_address = self.get_client_ip(self.context['request'])
        reviewer = self.context['request'].user
        validated_data.update({'ip_address': ip_address, 'reviewer': reviewer})
        return super(ReviewSerializer, self).create(validated_data)

    def get_client_ip(self, request):
        '''
        Method allow get the IP of client
        '''
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip
