# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from coding_python.reviews.models import Review, Company
from rest_framework.authtoken.admin import TokenAdmin


TokenAdmin.raw_id_fields = ('user',)

admin.site.register(Review)
admin.site.register(Company)
