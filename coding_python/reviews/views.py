# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets
from coding_python.reviews.serializers import UserSerializer, ReviewSerializer
from coding_python.reviews.serializers import CompanySerializer
from coding_python.reviews.models import Review, Company
from django.contrib.auth.models import User
from rest_framework.authentication import SessionAuthentication
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    '''
    API endpoint that allows users to be viewed.
    '''
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.filter(is_staff=False).order_by('-date_joined')
    serializer_class = UserSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    '''
    API endpoint that allows reviews to be viewed or edited.
    '''
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

    def get_queryset(self):
        user = self.request.user
        return Review.objects.filter(reviewer=user)


class CompanyViewSet(viewsets.ModelViewSet):
    '''
    API endpoint that allows companies to be viewed or edited.
    '''
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    base_name = 'company'
