# coding_python

A simple API that allows users to post and retrieve their reviews

# Install

The installation of the API can be done using a [virtualenv](http://virtualenvwrapper.readthedocs.io/en/latest/install.html) or directly in UNIX-based operating systems you can install the dependencies with the following command.

~~~~
pip install -r requirements.txt
~~~~

Then the command can be executed.

~~~~
python manage.py runserver 0.0.0.0:8000
~~~~

Finally, access the `localhost` or the server's IP wing where the installation was performed.

# Postman Test

use the file [`coding_baires_api.postman_collection.json`](coding_baires_api.postman_collection.json) to import the endpoints in [Postman](https://www.getpostman.com/).
In the loaded enpoints, change the IP to the server on which the API was configured.

![Alt text](postman_endpoints.png?raw=true "Postman Endpoints")

# Unit Test

To execute the unit tests, just execute the following command.

~~~~
python manage.py test
~~~~

If everything is ok, it should show a similar result to the image

![Alt text](unit_test.png?raw=true "Unit Test")
